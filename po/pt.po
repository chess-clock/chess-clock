# Portuguese translation for chess-clock.
# Copyright (C) 2023 chess-clock's COPYRIGHT HOLDER
# This file is distributed under the same license as the chess-clock package.
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: chess-clock main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/chess-clock/issues\n"
"POT-Creation-Date: 2023-07-02 16:17+0000\n"
"PO-Revision-Date: 2023-07-12 15:22+0100\n"
"Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>\n"
"Language-Team: Portuguese <pt@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"

#: data/com.clarahobbs.chessclock.desktop.in:3
#: data/com.clarahobbs.chessclock.appdata.xml.in:6 src/gtk/window.ui:6
msgid "Chess Clock"
msgstr "Relógio de Xadrez"

#: data/com.clarahobbs.chessclock.desktop.in:9
msgid "chess;game;clock;timer;"
msgstr "xadrez;jogo;relógio;temporizador;"

#: data/com.clarahobbs.chessclock.appdata.xml.in:7
msgid "Time games of over-the-board chess"
msgstr "Tempo de jogos de xadrez de tabuleiro"

#: data/com.clarahobbs.chessclock.appdata.xml.in:9
msgid ""
"Chess Clock is a simple application to provide time control for over-the-"
"board chess games. Intended for mobile use, players select the time control "
"settings desired for their game, then the black player taps their clock to "
"start white's timer. After each player's turn, they tap the clock to start "
"their opponent's, until the game is finished or one of the clocks reaches "
"zero."
msgstr ""
"O Relógio de Xadrez é uma aplicação simples que permite o controlo do tempo "
"para jogos de xadrez de tabuleiro. Destinado à utilização móvel, os "
"jogadores selecionam as definições de controlo de tempo desejadas para o seu "
"jogo, depois o jogador de peças pretas bate o seu relógio para iniciar o "
"temporizador do jogador de peças brancas. Após a vez de cada jogador, batem "
"no relógio para iniciar o do adversário, até o jogo estar terminado ou até "
"um dos relógios chegar a zero."

#: data/com.clarahobbs.chessclock.appdata.xml.in:18
msgid ""
"The main screen, showing timers for the white and black players of a chess "
"game"
msgstr ""
"O ecrã principal, mostrando temporizadores para os jogadores de peças "
"brancas e pretas de um jogo de xadrez"

#: data/com.clarahobbs.chessclock.appdata.xml.in:22
msgid "The time control selection screen"
msgstr "O ecrã de seleção do controlo de tempo"

#: data/com.clarahobbs.chessclock.appdata.xml.in:29
msgid "Clara Hobbs"
msgstr "Clara Hobbs"

#: src/gtk/timecontrolentry.ui:23
msgid "One minute per side, plus zero seconds per turn"
msgstr "Um minuto por lado, mais zero segundos por volta"

#: src/gtk/timecontrolentry.ui:36
msgid "Two minutes per side, plus one second per turn"
msgstr "Dois minutos por lado, mais um segundo por volta"

#: src/gtk/timecontrolentry.ui:49
msgid "Five minutes per side, plus zero seconds per turn"
msgstr "Cinco minutos por lado, mais zero segundos por volta"

#: src/gtk/timecontrolentry.ui:62
msgid "Five minutes per side, plus three seconds per turn"
msgstr "Cinco minutos por lado, mais três segundos por volta"

#: src/gtk/timecontrolentry.ui:75
msgid "Ten minutes per side, plus zero seconds per turn"
msgstr "Dez minutos por lado, mais zero segundos por volta"

#: src/gtk/timecontrolentry.ui:88
msgid "Ten minutes per side, plus five seconds per turn"
msgstr "Dez minutos por lado, mais cinco segundos por volta"

#: src/gtk/timecontrolentry.ui:101
msgid "Thirty minutes per side, plus zero seconds per turn"
msgstr "Trinta minutos por lado, mais zero segundos por volta"

#: src/gtk/timecontrolentry.ui:114
msgid "Thirty minutes per side, plus twenty seconds per turn"
msgstr "Trinta minutos por lado, mais vinte segundos por volta"

#: src/gtk/timecontrolentry.ui:134
msgid "Main time minutes"
msgstr "Minutos de tempo principal"

#: src/gtk/timecontrolentry.ui:153
msgid "Main time seconds"
msgstr "Segundos de tempo principal"

#: src/gtk/timecontrolentry.ui:171
msgid "Increment seconds"
msgstr "Incrementar segundos"

#: src/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "Game"
msgstr "Jogo"

#: src/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "New Time Control"
msgstr "Novo controlo de tempo"

#: src/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Restart Timer"
msgstr "Reiniciar temporizador"

#: src/gtk/help-overlay.ui:28
msgctxt "shortcut window"
msgid "General"
msgstr "Geral"

#: src/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Open Main Menu"
msgstr "Abrir menu principal"

#: src/gtk/help-overlay.ui:37
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Mostrar atalhos"

#: src/gtk/help-overlay.ui:43
msgctxt "shortcut window"
msgid "Close Window"
msgstr "Fechar janela"

#: src/gtk/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Quit"
msgstr "Sair"

#: src/gtk/window.ui:15
msgctxt "a11y"
msgid "Select time controls"
msgstr "Selecionar controlos de tempo"

#: src/gtk/window.ui:44
msgid "Welcome to Chess Clock"
msgstr "Bem-vindo ao Relógio de Xadrez"

#: src/gtk/window.ui:54
msgid "Select time controls below to get started"
msgstr "Selecione os controlos de tempo abaixo para começar"

#: src/gtk/window.ui:72
msgid "Time control method"
msgstr "Método de controlo de tempo"

#: src/gtk/window.ui:76
msgid "Increment"
msgstr "Incrementar"

#: src/gtk/window.ui:77
msgid "Bronstein delay"
msgstr "Atraso de Bronstein"

#: src/gtk/window.ui:78
msgid "Simple delay"
msgstr "Atraso simples"

#: src/gtk/window.ui:90
msgid "Start Game"
msgstr "Iniciar o jogo"

#: src/gtk/window.ui:109
msgctxt "a11y"
msgid "Playing"
msgstr "A jogar"

#: src/gtk/window.ui:168 src/gtk/window.ui:282
msgid "Main Menu"
msgstr "Menu principal"

#: src/gtk/window.ui:182 src/gtk/window.ui:296
msgid "Toggle Pause"
msgstr "Pausa"

#: src/gtk/window.ui:200 src/gtk/window.ui:314
msgctxt "a11y"
msgid "Player white timer"
msgstr "Temporizador de jogador de peças brancas"

#: src/gtk/window.ui:211 src/gtk/window.ui:326
msgctxt "a11y"
msgid "Player black timer"
msgstr "Temporizador de jogador de peças pretas"

#: src/gtk/window.ui:348
msgid "_New Time Control"
msgstr "Novo controlo de tempo"

#: src/gtk/window.ui:352
msgid "_Restart Timer"
msgstr "_Reiniciar temporizador"

#: src/gtk/window.ui:358
msgid "_Mute Alert Sound"
msgstr "_Silenciar som de alerta"

#: src/gtk/window.ui:364
msgid "_Keyboard Shortcuts"
msgstr "_Teclas de atalho"

#: src/gtk/window.ui:368
msgid "_About Chess Clock"
msgstr "_Acerca do Relógio de Xadrez"

#: src/main.py:64
msgid "translator-credits"
msgstr "Hugo Carvalho <hugokarvalho@hotmail.com>"

#: src/timerbutton.py:55
msgid "Player white timer"
msgstr "Temporizador de jogador de peças brancas"

#: src/timerbutton.py:60
msgid "Player black timer"
msgstr "Temporizador de jogador de peças pretas"

#~ msgid "Custom"
#~ msgstr "Personalizar"

#~ msgctxt "shortcut window"
#~ msgid "Open Primary Menu"
#~ msgstr "Abrir menu principal"

#~ msgid "Three minutes per side, plus zero seconds per turn"
#~ msgstr "Três minutos por lado, mais zero segundos por volta"

#~ msgid "Three minutes per side, plus two seconds per turn"
#~ msgstr "Três minutos por lado, mais dois segundos por volta"

#~ msgid "Fifteen minutes per side, plus ten seconds per turn"
#~ msgstr "Quinze minutos por lado, mais dez segundos por volta"

#~ msgid "Primary Menu"
#~ msgstr "Menu principal"

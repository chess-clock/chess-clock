Name:           chess-clock
Version:        0.6.0
Release:        alt1
Summary:        Mobile-friendly chess timer app for over-the-board games with customizable time controls
Group:          Games/Boards
BuildArch:      noarch

License:        GPL-3.0
URL:            https://gitlab.gnome.org/World/chess-clock
Source0:        %{name}-%{version}.tar

BuildRequires(pre): rpm-macros-meson
BuildRequires(pre): rpm-build-python3
BuildRequires: cmake
BuildRequires: meson
BuildRequires: libgsound-devel
BuildRequires: libadwaita-devel
BuildRequires: glib2-devel
BuildRequires: libgtk4-devel

Requires: libadwaita
Requires: gsound
Requires: python3
Requires: python3(gettext)
Requires: python3(locale)
Requires: python3(os)
Requires: python3(signal)
Requires: typelib(Adw)
Requires: typelib(GLib)
Requires: typelib(GObject)
Requires: typelib(GSound)
Requires: typelib(Gio)
Requires: typelib(Gtk)

%description
Chess Clock is a simple application to provide time control for over-the-board
chess games.  Intended for mobile use, players select the time control settings
desired for their game, then the black player taps their clock to start white's
timer.  After each player's turn, they tap the clock to start their opponent's,
until the game is finished or one of the clocks reaches zero.

%prep
%setup -v

%build
%meson
%meson_build


%install
%meson_install
%find_lang %{name}

%files -f %{name}.lang
/usr/bin/chess-clock
/usr/share/appdata/com.clarahobbs.chessclock.appdata.xml
/usr/share/applications/com.clarahobbs.chessclock.desktop
/usr/share/chess-clock/
/usr/share/glib-2.0/schemas/com.clarahobbs.chessclock.gschema.xml
/usr/share/icons/hicolor/scalable/apps/com.clarahobbs.chessclock.svg
/usr/share/icons/hicolor/symbolic/apps/com.clarahobbs.chessclock-symbolic.svg

%changelog
* Thu Mar 26 2024 Aleksandr A. Voyt <vojtaa@basealt.ru> 0.6.0-alt1
- First package version
